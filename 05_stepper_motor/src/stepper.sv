/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module stepper (
  input  wire  clk,
  input  wire  rst,
  input  wire  ce,
  input  wire  dir,
  output logic ap,
  output logic am,
  output logic bp,
  output logic bm
);

  typedef enum logic [1:0] {
    STEP0 = 2'b00,
    STEP1 = 2'b01,
    STEP2 = 2'b10,
    STEP3 = 2'b11
  } state_t;

  state_t state_c;
  state_t state_r;

  logic   ap_c;
  logic   am_c;
  logic   bp_c;
  logic   bm_c;

  always_comb
    case (state_r)
      STEP0:   state_c = dir ? STEP1 : STEP3;
      STEP1:   state_c = dir ? STEP2 : STEP0;
      STEP2:   state_c = dir ? STEP3 : STEP1;
      STEP3:   state_c = dir ? STEP0 : STEP2;
      default: state_c = STEP0;
    endcase

  always_ff @(posedge clk)
    if (rst) state_r <= STEP0;
    else if (ce) state_r <= state_c;

  assign {ap_c, am_c, bp_c, bm_c} =
   (state_r == STEP0) ? {1'b1, 1'b1, 1'b0, 1'b0} :
   (state_r == STEP1) ? {1'b0, 1'b1, 1'b1, 1'b0} :
   (state_r == STEP2) ? {1'b0, 1'b0, 1'b1, 1'b1} :
                        {1'b1, 1'b0, 1'b0, 1'b1};

  always_ff @(posedge clk)
    if (rst) {ap, am, bp, bm} <= '0;
    else {ap, am, bp, bm} <= {ap_c, am_c, bp_c, bm_c};

endmodule

`default_nettype wire
