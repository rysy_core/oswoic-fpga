/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module top_tb;
  logic clk;
  logic a_in;
  logic b_in;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    a_in = 1'b0;
    b_in = 1'b1;
    @(posedge clk) a_in = 1'b1;
    repeat (15) @(posedge clk);
    b_in = 1'b0;
    repeat (15) @(posedge clk);
    $stop;
  end

  top #(
    .FCLK(200)
  ) dut (
    .clk (clk),
    .a_in(a_in),
    .b_in(b_in),
    .ap  (),
    .am  (),
    .bp  (),
    .bm  ()
  );

endmodule

`default_nettype wire
