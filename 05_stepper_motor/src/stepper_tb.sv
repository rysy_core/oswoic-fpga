/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module stepper_tb;
  logic clk;
  logic rst;
  logic ce;
  logic dir;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst = 1'b1;
    dir = 1'b1;
    ce  = 1'b0;
    repeat (2) @(posedge clk);
    rst = 1'b0;

    repeat (6) begin
      ce = 1'b1;
      @(posedge clk);
      ce = 1'b0;
      @(posedge clk);
    end

    dir = 1'b0;

    repeat (6) begin
      ce = 1'b1;
      @(posedge clk);
      ce = 1'b0;
      @(posedge clk);
    end

    $stop;
  end

  stepper dut (
    .clk(clk),
    .rst(rst),
    .ce (ce),
    .dir(dir),
    .ap (),
    .am (),
    .bp (),
    .bm ()
  );

endmodule

`default_nettype wire
