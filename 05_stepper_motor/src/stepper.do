#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog stepper.sv
vlog stepper_tb.sv

vsim work.stepper_tb

add wave -divider "inputs"
add wave sim:/stepper_tb/clk
add wave sim:/stepper_tb/rst
add wave sim:/stepper_tb/ce
add wave sim:/stepper_tb/dir

add wave -divider "state"
add wave sim:/stepper_tb/dut/state_c
add wave sim:/stepper_tb/dut/state_r

add wave -divider "outputs"
add wave sim:/stepper_tb/dut/ap
add wave sim:/stepper_tb/dut/am
add wave sim:/stepper_tb/dut/bp
add wave sim:/stepper_tb/dut/bm

run -all
wave zoom full
