#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../02_counter/src/counter.sv
vlog top.sv
vlog top_tb.sv

vsim work.top_tb

add wave -divider "inputs"
add wave sim:/top_tb/clk
add wave sim:/top_tb/a_in
add wave sim:/top_tb/b_in

add wave -divider "inside"
add wave sim:/top_tb/dut/dir
add wave sim:/top_tb/dut/ce
add wave sim:/top_tb/dut/s/state_c
add wave sim:/top_tb/dut/s/state_r

add wave -divider "outputs"
add wave sim:/top_tb/dut/ap
add wave sim:/top_tb/dut/am
add wave sim:/top_tb/dut/bp
add wave sim:/top_tb/dut/bm

run -all
wave zoom full
