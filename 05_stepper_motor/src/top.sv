/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module top #(
  parameter FCLK = 27000000
) (
  input  wire  clk,
  input  wire  a_in,
  input  wire  b_in,
  output logic ap,
  output logic am,
  output logic bp,
  output logic bm
);
  logic rst;
  logic dir;
  logic ce;

  always_ff @(posedge clk) rst <= ~a_in;

  always_ff @(posedge clk)
    if (rst) dir <= '1;
    else dir <= b_in;

  counter #(
    .N(FCLK / 50)
  ) c (
    .clk(clk),
    .rst(rst),
    .ce (1'b1),
    .q  (),
    .ov (ce)
  );

  stepper s (
    .clk(clk),
    .rst(rst),
    .ce (ce),
    .dir(dir),
    .ap (ap),
    .am (am),
    .bp (bp),
    .bm (bm)
  );

endmodule

`default_nettype wire
