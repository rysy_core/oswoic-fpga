/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module edge_detector_tb;
  logic clk;
  logic rst;
  logic d;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst = 1'b1;
    d   = 1'b1;
    @(posedge clk);
    rst = 1'b0;
    repeat (5) @(posedge clk);
    d = 1'b0;
    repeat (5) @(posedge clk);
    d = 1'b1;
    repeat (5) @(posedge clk);
    d = 1'b0;
    repeat (4) @(posedge clk);
    $stop;
  end

  edge_detector dut (
    .clk(clk),
    .rst(rst),
    .d  (d),
    .q  ()
  );

endmodule

`default_nettype wire
