#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog edge_detector.sv
vlog edge_detector_tb.sv

vsim work.edge_detector_tb

add wave -divider inputs
add wave sim:/edge_detector_tb/clk
add wave sim:/edge_detector_tb/rst
add wave sim:/edge_detector_tb/d

add wave -divider "module inside"
add wave sim:/edge_detector_tb/dut/dr
add wave sim:/edge_detector_tb/dut/q_c

add wave -divider outputs
add wave sim:/edge_detector_tb/dut/q

run -all
wave zoom full