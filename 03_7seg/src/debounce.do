#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog debounce.sv
vlog debounce_tb.sv

vsim work.debounce_tb

add wave -divider inputs
add wave sim:/debounce_tb/clk
add wave sim:/debounce_tb/rst
add wave sim:/debounce_tb/d

add wave -divider "module inside"
add wave sim:/debounce_tb/dut/dr
add wave -unsigned sim:/debounce_tb/dut/stable_time_c
add wave -unsigned sim:/debounce_tb/dut/stable_time
add wave sim:/debounce_tb/dut/stable_time[2]

add wave -divider outputs
add wave -unsigned sim:/debounce_tb/dut/q

run -all
wave zoom full