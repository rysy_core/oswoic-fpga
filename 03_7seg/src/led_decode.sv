/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module led_decode (
  input wire clk,
  input wire rst,
  input wire [3:0] bdc,
  output logic [6:0] leds
);
  logic [6:0] leds_c;

  assign leds_c = (bdc == 4'd0) ? 7'b1000000 :
	                (bdc == 4'd1) ? 7'b1111001 :
					(bdc == 4'd2) ? 7'b0100100 :
					(bdc == 4'd3) ? 7'b0110000 :
					(bdc == 4'd4) ? 7'b0011001 :
					(bdc == 4'd5) ? 7'b0010010 :
					(bdc == 4'd6) ? 7'b0000010 :
					(bdc == 4'd7) ? 7'b1111000 :
					(bdc == 4'd8) ? 7'b0000000 :
					(bdc == 4'd9) ? 7'b0010000 :
					                7'b1111111;

  always_ff @(posedge clk)
    if (rst) leds <= '1;
    else leds <= leds_c;

endmodule

`default_nettype wire
