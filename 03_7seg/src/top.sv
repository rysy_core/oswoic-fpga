/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module top #(
  parameter DEBOUNCE_N = 20
) (
  input wire clk,
  input wire a_in,
  input wire b_in,
  output logic [7:0] leds
);
  logic rst;
  logic b;
  logic b_db;
  logic ce;
  logic [3:0] c_out;
  logic [6:0] leds_decode;
  logic ov;
  logic ov_r;

  always_ff @(posedge clk) rst <= ~a_in;

  always_ff @(posedge clk) b <= b_in;

  debounce #(
    .N(DEBOUNCE_N)
  ) db (
    .clk(clk),
    .rst(rst),
    .d  (b),
    .q  (b_db)
  );

  edge_detector ed (
    .clk(clk),
    .rst(rst),
    .d  (b_db),
    .q  (ce)
  );

  counter #(
    .N(10)
  ) c (
    .clk(clk),
    .rst(rst),
    .ce (ce),
    .q  (c_out),
    .ov (ov)
  );

  led_decode ld (
    .clk (clk),
    .rst (rst),
    .bdc (c_out),
    .leds(leds_decode)
  );

  always_ff @(posedge clk) ov_r <= ~ov;

  assign leds = {ov_r, leds_decode};

endmodule

`default_nettype wire
