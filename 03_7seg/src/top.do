#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../02_counter/src/counter.sv
vlog debounce.sv
vlog edge_detector.sv
vlog top.sv
vlog top_tb.sv

vsim work.top_tb

add wave -divider inputs
add wave sim:/top_tb/clk
add wave sim:/top_tb/a_in
add wave sim:/top_tb/b_in

add wave -divider "module inside"
add wave sim:/top_tb/dut/rst
add wave sim:/top_tb/dut/b
add wave sim:/top_tb/dut/b_db
add wave sim:/top_tb/dut/ce
add wave -unsigned sim:/top_tb/dut/c_out
add wave sim:/top_tb/dut/leds_decode
add wave sim:/top_tb/dut/ov

add wave -divider outputs
add wave sim:/top_tb/dut/leds

run -all
wave zoom full