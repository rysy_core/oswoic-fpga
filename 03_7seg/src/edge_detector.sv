/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module edge_detector (
  input  wire  clk,
  input  wire  rst,
  input  wire  d,
  output logic q
);
  logic dr;
  logic q_c;

  always_ff @(posedge clk)
    if (rst) dr <= '0;
    else dr <= d;

  assign q_c = (dr == 1'd1) && (d == 1'd0);

  always_ff @(posedge clk)
    if (rst) q <= '0;
    else q <= q_c;

endmodule

`default_nettype wire
