#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog led_decode.sv
vlog led_decode_tb.sv

vsim work.led_decode_tb

add wave -divider inputs
add wave sim:/led_decode_tb/clk
add wave sim:/led_decode_tb/rst
add wave sim:/led_decode_tb/bdc

add wave -divider "module inside"
add wave sim:/led_decode_tb/dut/leds_c

add wave -divider outputs
add wave sim:/led_decode_tb/dut/leds

run -all
wave zoom full