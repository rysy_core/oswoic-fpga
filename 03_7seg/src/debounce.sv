/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module debounce #(
  parameter N = 8
) (
  input  wire  clk,
  input  wire  rst,
  input  wire  d,
  output logic q
);
  logic [N:0] stable_time_c;
  logic [N-1:0] stable_time;
  logic dr;

  always_ff @(posedge clk)
    if (rst) dr <= '0;
    else dr <= d;

  assign stable_time_c = (d != dr) ? '0 : stable_time + 1'd1;

  always_ff @(posedge clk)
    if (rst) stable_time <= '0;
    else stable_time <= stable_time_c[N-1:0];

  always_ff @(posedge clk)
    if (rst) q <= '0;
    else if (stable_time[N-1]) q <= dr;

endmodule

`default_nettype wire
