#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog counter.sv
vlog counter_tb.sv

vsim work.counter_tb

add wave -divider inputs
add wave sim:/counter_tb/clk
add wave sim:/counter_tb/rst
add wave sim:/counter_tb/ce

add wave -divider outputs
add wave -unsigned sim:/counter_tb/dut/q
add wave sim:/counter_tb/dut/ov

run -all
wave zoom full