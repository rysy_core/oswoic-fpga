#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog counter.sv
vlog cnt_led.sv
vlog cnt_led_tb.sv

vsim work.cnt_led_tb

add wave -divider inputs
add wave sim:/cnt_led_tb/clk
add wave sim:/cnt_led_tb/a_in
add wave sim:/cnt_led_tb/b_in

add wave -divider outputs
add wave -unsigned sim:/cnt_led_tb/dut/red
add wave -unsigned sim:/cnt_led_tb/dut/green
add wave -unsigned sim:/cnt_led_tb/dut/blue

run -all
wave zoom full