/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module cnt_led (
  input  wire  clk,
  input  wire  a_in,
  input  wire  b_in,
  output logic red,
  output logic green,
  output logic blue
);
  logic ov;
  logic ce;
  logic [2:0] c_out;
  logic rst;

  always_ff @(posedge clk)
    rst <= ~a_in;

  always_ff @(posedge clk)
    ce <= b_in;

  // clk is 27 MHz
  counter #(
    .N(27000000)
  ) c0 (
    .clk(clk),
    .rst(rst),
    .ce (ce),
    .q  (),
    .ov (ov)
  );

  counter #(
    .N(8)
  ) c1 (
    .clk(clk),
    .rst(rst),
    .ce (ce & ov),
    .q  (c_out),
    .ov ()
  );

  always_ff @(posedge clk)
    if (rst)
      {blue, green, red} <= '1;
    else
      {blue, green, red} <= ~c_out;

endmodule

`default_nettype wire
