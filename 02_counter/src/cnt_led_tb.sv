/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module cnt_led_tb;
  logic clk;
  logic a_in;
  logic b_in;

  initial begin
    clk = 1'b0;
    forever #5ns clk = ~clk;
  end

  initial begin
    a_in = 1'b0;
    b_in = 1'b1;
    repeat (3) @(posedge clk);
    a_in = 1'b1;
    repeat (100) @(posedge clk);
    b_in = 1'b0;
    repeat (40) @(posedge clk);
    b_in = 1'b1;
    repeat (40) @(posedge clk);
    a_in = 1'b0;
    repeat (40) @(posedge clk);
    a_in = 1'b1;
    repeat (40) @(posedge clk);
    $stop;
  end

  cnt_led dut (
    .clk  (clk),
    .a_in (a_in),
    .b_in (b_in),
    .red  (),
    .green(),
    .blue ()
  );
  defparam dut.c0.N = 10;

endmodule

`default_nettype wire
