/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

// N > 2 

module counter #(
  parameter N = 25,
  parameter W = $clog2(N - 1)
) (
  input  wire          clk,
  input  wire          rst,
  input  wire          ce,
  output logic [W-1:0] q,
  output logic         ov
);
  logic [W:0] q_c;
  logic ov_c;

  assign ov_c = (q == N - 2);

  always_ff @(posedge clk)
    if (rst)
      ov <= '0;
    else if (ce)
      ov <= ov_c;

  assign q_c = ov ? '0 : q + 1'b1;

  always_ff @(posedge clk)
    if (rst)
      q <= '0;
    else if (ce)
      q <= q_c[W-1:0];

endmodule

`default_nettype wire
