/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module counter_tb;
  logic clk, rst, ce;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst = 1'b1;
    ce  = 1'b1;
    @(posedge clk);
    rst = 1'b0;
    repeat (6) @(posedge clk);
    ce = 1'b0;
    repeat (3) @(posedge clk);
    ce = 1'b1;
    repeat (2) @(posedge clk);
    #2 rst = 1'b1;
    @(posedge clk);
    #2 rst = 1'b0;
    repeat (3) @(posedge clk);
    $stop;
  end

  counter #(
    .N(5)
  ) dut (
    .clk(clk),
    .rst(rst),
    .ce (ce),
    .q  (),
    .ov ()
  );

endmodule

`default_nettype wire
