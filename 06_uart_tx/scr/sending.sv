/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module sending #(
  parameter FCLK = 27000000
) (
  input  wire  clk,
  input  wire  a_in,
  input  wire  b_in,
  output logic tx
);
  logic rst;
  logic [7:0] data;
  logic ready;
  logic valid;

  always_ff @(posedge clk) rst <= ~a_in;

  always_ff @(posedge clk)
    if (rst) valid <= 1'b0;
    else valid <= b_in;

  counter #(
    .N(256)
  ) c (
    .clk(clk),
    .rst(rst),
    .ce (ready),
    .q  (data),
    .ov ()
  );

  uart_tx #(
    .FCLK(FCLK),
    .BAUD(115200)
  ) utx (
    .clk(clk),
    .rst(rst),
    .data(data),
    .valid(valid),
    .ready(ready),
    .tx(tx)
  );

endmodule

`default_nettype wire
