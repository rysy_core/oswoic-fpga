#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../02_counter/src/counter.sv
vlog uart_tx.sv
vlog uart_tx_tb.sv

vsim work.uart_tx_tb

add wave -divider "inputs"
add wave sim:/uart_tx_tb/dut/clk
add wave sim:/uart_tx_tb/dut/rst
add wave sim:/uart_tx_tb/dut/data
add wave sim:/uart_tx_tb/dut/valid

add wave -divider "state"
add wave sim:/uart_tx_tb/dut/handshake
add wave sim:/uart_tx_tb/dut/state_c
add wave sim:/uart_tx_tb/dut/state_r
add wave sim:/uart_tx_tb/dut/data_to_send_c
add wave sim:/uart_tx_tb/dut/data_to_send_r
add wave sim:/uart_tx_tb/dut/c0_ce
add wave sim:/uart_tx_tb/dut/tic
add wave sim:/uart_tx_tb/dut/c1/ce
add wave sim:/uart_tx_tb/dut/c1/q
add wave sim:/uart_tx_tb/dut/send_end

add wave -divider "outputs"
add wave sim:/uart_tx_tb/dut/tx
add wave sim:/uart_tx_tb/dut/ready

run -all
wave zoom full
