/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module uart_tx #(
  parameter FCLK = 27000000,
  parameter BAUD = 115200
) (
  input wire clk,
  input wire rst,
  input wire [7:0] data,
  input wire valid,
  output logic ready,
  output logic tx
);

  typedef enum logic {
    IDLE = 1'b0,
    SEND = 1'b1
  } state_t;

  state_t state_c;
  state_t state_r;
  logic handshake;
  logic [9:0] data_to_send_c;
  logic [9:0] data_to_send_r;
  logic c0_ce;
  logic data_to_send_ce;
  logic tic;
  logic c1_ce;
  logic c1_ov;
  logic send_end;

  assign handshake = valid & ready;

  assign state_c = (handshake) ? SEND :
                    (send_end) ? IDLE :
                                 state_r;

  always_ff @(posedge clk)
    if (rst) state_r <= IDLE;
    else state_r <= state_c;

  assign data_to_send_c = (handshake) ?
    {1'd1, data, 1'd0} :
    {1'd1, data_to_send_r[9:1]};

  assign data_to_send_ce = handshake | tic;

  always_ff @(posedge clk)
    if (rst) data_to_send_r <= '1;
    else if (data_to_send_ce)
      data_to_send_r <= data_to_send_c;

  assign c0_ce = (state_r == SEND);

  counter #(
    .N((FCLK+BAUD/2)/BAUD)
  ) c0 (
    .clk(clk),
    .rst(rst),
    .ce (c0_ce),
    .q  (),
    .ov (tic)
  );

  assign c1_ce = c0_ce & tic;

  counter #(
    .N(10)
  ) c1 (
    .clk(clk),
    .rst(rst),
    .ce (c1_ce),
    .q  (),
    .ov (c1_ov)
  );

  assign send_end = c1_ov & c1_ce;

  assign tx = data_to_send_r[0];
  assign ready = (state_r == IDLE);

endmodule

`default_nettype wire
