/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module uart_tx_tb;
  logic clk;
  logic rst;
  logic [7:0] data;
  logic valid;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst   = 1'b1;
    data  = 8'd0;
    valid = 1'b0;
    repeat (2) @(posedge clk);
    rst = 1'b0;
    repeat (2) @(posedge clk);

    valid = 1'b1;
    @(posedge clk);
    valid = 1'b0;
    repeat (2500) @(posedge clk);

    $stop;
  end

  uart_tx #(
    .FCLK(27000000),
    .BAUD(115200)
  ) dut (
    .clk(clk),
    .rst(rst),
    .data(data),
    .valid(valid),
    .ready(),
    .tx()
  );

endmodule

`default_nettype wire
