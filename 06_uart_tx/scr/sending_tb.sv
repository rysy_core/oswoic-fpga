/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module sending_tb;
  logic clk;
  logic a_in;
  logic b_in;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    a_in = 1'b0;
    b_in = 1'b1;
    @(posedge clk) a_in = 1'b1;
    repeat (700000) @(posedge clk);
    $stop;
  end

  sending #(
    .FCLK(27000000)
  ) dut (
    .clk (clk),
    .a_in(a_in),
    .b_in(b_in),
    .tx  ()
  );

endmodule

`default_nettype wire
