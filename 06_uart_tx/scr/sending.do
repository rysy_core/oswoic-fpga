#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../02_counter/src/counter.sv
vlog uart_tx.sv
vlog sending.sv
vlog sending_tb.sv

vsim work.sending_tb

add wave -divider "inputs"
add wave sim:/sending_tb/dut/clk
add wave sim:/sending_tb/dut/a_in
add wave sim:/sending_tb/dut/b_in

add wave -divider "state"
add wave sim:/sending_tb/dut/utx/handshake
add wave sim:/sending_tb/dut/utx/state_c
add wave sim:/sending_tb/dut/utx/state_r
add wave sim:/sending_tb/dut/utx/data_to_send_c
add wave sim:/sending_tb/dut/utx/data_to_send_r
add wave sim:/sending_tb/dut/utx/c0_ce
add wave sim:/sending_tb/dut/utx/tic
add wave sim:/sending_tb/dut/utx/c1/ce
add wave sim:/sending_tb/dut/utx/c1/q
add wave sim:/sending_tb/dut/utx/send_end
add wave sim:/sending_tb/dut/ready

add wave -divider "outputs"
add wave sim:/sending_tb/dut/tx

run -all
wave zoom full
