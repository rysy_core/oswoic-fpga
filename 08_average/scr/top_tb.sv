/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module top_tb;
  parameter FCLK = 27000000;
  parameter BAUD = 115200;

  logic clk;
  logic rst;
  logic [7:0] data;
  logic valid;
  logic tx;
  logic rx;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst   = 1'b1;
    data  = 8'd120;
    valid = 1'b0;
    repeat (2) @(posedge clk);
    rst = 1'b0;
    repeat (2) @(posedge clk);

    valid = 1'b1;
    @(posedge clk);
    valid = 1'b0;
    repeat (5000) @(posedge clk);

    $stop;
  end

  uart_tx #(
    .FCLK(FCLK),
    .BAUD(BAUD)
  ) utx (
    .clk(clk),
    .rst(rst),
    .data(data),
    .valid(valid),
    .ready(),
    .tx(tx)
  );

  top #(
    .FCLK(FCLK)
  ) dut (
    .clk (clk),
    .a_in(!rst),
    .rx  (tx),
    .tx  (rx)
  );

  uart_rx #(
    .FCLK(FCLK),
    .BAUD(BAUD)
  ) urx (
    .clk(clk),
    .rst(rst),
    .rx(rx),
    .data(),
    .valid()
  );

endmodule

`default_nettype wire
