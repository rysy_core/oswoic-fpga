//Copyright (C)2014-2023 GOWIN Semiconductor Corporation.
//All rights reserved.
//File Title: Timing Constraints file
create_clock -name clk -period 37.037 -waveform {0 18.518} [get_ports {clk}]
