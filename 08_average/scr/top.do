#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../02_counter/src/counter.sv
vlog ../../06_uart_tx/scr/uart_tx.sv
vlog ../../07_uart_rx/scr/uart_rx.sv
vlog average.sv
vlog top.sv
vlog top_tb.sv

vsim work.top_tb

add wave -divider "inputs"
add wave sim:/top_tb/dut/clk
add wave sim:/top_tb/dut/a_in

add wave -divider "send"
add wave -unsigned sim:/top_tb/utx/data
add wave sim:/top_tb/utx/valid
add wave sim:/top_tb/utx/tx

add wave -divider "module"
add wave -unsigned sim:/top_tb/dut/data_rx
add wave sim:/top_tb/dut/valid_rx
add wave -unsigned sim:/top_tb/dut/data_avg
add wave sim:/top_tb/dut/valid_avg

add wave -divider "outputs"
add wave sim:/top_tb/urx/rx
add wave -unsigned sim:/top_tb/urx/data
add wave sim:/top_tb/urx/valid

run -all
wave zoom full
