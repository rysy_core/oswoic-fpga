/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module top #(
  parameter FCLK = 27000000
) (
  input  wire  clk,
  input  wire  a_in,
  input  wire  rx,
  output logic tx
);
  parameter BAUD = 115200;

  logic rst;
  logic [7:0] data_rx;
  logic valid_rx;
  logic [7:0] data_avg;
  logic valid_avg;

  always_ff @(posedge clk) rst <= ~a_in;

  uart_rx #(
    .FCLK(FCLK),
    .BAUD(BAUD)
  ) urx (
    .clk(clk),
    .rst(rst),
    .rx(rx),
    .data(data_rx),
    .valid(valid_rx)
  );

  average avg (
    .clk(clk),
    .rst(rst),
    .data_in(data_rx),
    .valid_in(valid_rx),
    .data_out(data_avg),
    .valid_out(valid_avg)
  );

  uart_tx #(
    .FCLK(FCLK),
    .BAUD(BAUD)
  ) utx (
    .clk(clk),
    .rst(rst),
    .data(data_avg),
    .valid(valid_avg),
    .ready(),
    .tx(tx)
  );

endmodule

`default_nettype wire
