#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog average.sv
vlog average_tb.sv

vsim work.average_tb

add wave -divider "input"
add wave sim:/average_tb/dut/clk
add wave sim:/average_tb/dut/rst
add wave -unsigned sim:/average_tb/dut/data_in
add wave sim:/average_tb/dut/valid_in

add wave -divider "inside"
add wave sim:/average_tb/dut/valid_in_r
add wave -unsigned sim:/average_tb/dut/data_in_r
add wave -unsigned sim:/average_tb/dut/data_last_r
add wave -unsigned sim:/average_tb/dut/sum_c

add wave -divider "output"
add wave -unsigned sim:/average_tb/dut/data_out
add wave sim:/average_tb/dut/valid_out

run -all
wave zoom full
