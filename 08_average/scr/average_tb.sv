/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module average_tb;
  logic clk;
  logic rst;
  logic [7:0] data;
  logic valid;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst   = 1'b1;
    data  = 8'd0;
    valid = 1'b0;
    @(posedge clk);
    rst   = 1'b0;
    valid = 1'b1;
    data  = 8'd10;
    @(posedge clk);
    valid = 1'b0;
    data  = 8'd23;
    @(posedge clk);
    valid = 1'b1;
    data  = 8'd20;
    @(posedge clk);
    valid = 1'b0;
    data  = 8'd99;
    repeat (2) @(posedge clk);
    valid = 1'b1;
    data  = 8'd40;
    @(posedge clk);
    valid = 1'b0;
    data  = 8'd0;
    repeat (3) @(posedge clk);
    $stop;
  end

  average dut (
    .clk(clk),
    .rst(rst),
    .data_in(data),
    .valid_in(valid),
    .data_out(),
    .valid_out()
  );

endmodule

`default_nettype wire
