/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module average (
  input wire clk,
  input wire rst,
  input wire [7:0] data_in,
  input wire valid_in,
  output logic [7:0] data_out,
  output logic valid_out
);

  logic valid_in_r;
  logic [7:0] data_in_r;
  logic [7:0] data_last_r;
  logic [8:0] sum_c;

  always_ff @(posedge clk)
    if (rst) valid_in_r <= '0;
    else valid_in_r <= valid_in;

  always_ff @(posedge clk)
    if (rst) data_in_r <= '0;
    else data_in_r <= data_in;

  always_ff @(posedge clk)
    if (rst) data_last_r <= '0;
    else if (valid_in_r) data_last_r <= data_in_r;

  assign sum_c = data_in_r + data_last_r;

  always_ff @(posedge clk)
    if (rst) valid_out <= '0;
    else valid_out <= valid_in_r;

  always_ff @(posedge clk)
    if (rst) data_out <= '0;
    else data_out <= sum_c[8:1];

endmodule

`default_nettype wire
