/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module leds (
  input  wire  clk,
  input  wire  a_in,
  input  wire  b_in,
  output logic red,
  output logic green,
  output logic blue
);

  logic a, b, red_c, green_c, blue_c;

  assign a = ~a_in;
  assign b = ~b_in;

  assign red_c = ~(a & b);
  assign green_c = ~(a | b);
  assign blue_c = ~(a ^ b);

  always_ff @(posedge clk) begin
    red   <= red_c;
    green <= green_c;
    blue  <= blue_c;
  end

endmodule

`default_nettype wire
