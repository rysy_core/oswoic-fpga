/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module leds_tb;

  logic clk, a_in, b_in;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    a_in = 1'b1;
    b_in = 1'b1;
    @(posedge clk);
    a_in = 1'b1;
    b_in = 1'b0;
    @(posedge clk);
    a_in = 1'b0;
    b_in = 1'b1;
    @(posedge clk);
    a_in = 1'b0;
    b_in = 1'b0;
    @(posedge clk);
    @(posedge clk);
    $stop;
  end

  leds dut (
    .clk  (clk),
    .a_in (a_in),
    .b_in (b_in),
    .red  (),
    .green(),
    .blue ()
  );

endmodule

`default_nettype wire
