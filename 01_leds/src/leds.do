#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog leds.sv
vlog leds_tb.sv

vsim work.leds_tb

add wave sim:/leds_tb/clk
add wave sim:/leds_tb/a_in
add wave sim:/leds_tb/b_in

add wave -divider inside
add wave sim:/leds_tb/dut/a
add wave sim:/leds_tb/dut/b
add wave sim:/leds_tb/dut/red_c
add wave sim:/leds_tb/dut/green_c
add wave sim:/leds_tb/dut/blue_c

add wave -divider outputs
add wave sim:/leds_tb/dut/red
add wave sim:/leds_tb/dut/green
add wave sim:/leds_tb/dut/blue

run -all
wave zoom full