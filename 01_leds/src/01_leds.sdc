//Copyright (C)2014-2023 GOWIN Semiconductor Corporation.
//All rights reserved.
//File Title: Timing Constraints file
//GOWIN Version: 1.9.8.10 
//Created Time: 2023-01-30 23:00:38
create_clock -name clk -period 37.037 -waveform {0 18.518} [get_ports {clk}]
