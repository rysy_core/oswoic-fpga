/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module uart_rx #(
  parameter FCLK = 27000000,
  parameter BAUD = 115200
) (
  input wire clk,
  input wire rst,
  input wire rx,
  output logic [7:0] data,
  output logic valid
);

  parameter N = (FCLK + BAUD / 2) / BAUD;
  parameter W = $clog2(N - 1);

  typedef enum logic {
    IDLE = 1'b0,
    RECEIVE = 1'b1
  } state_t;

  state_t state_c;
  state_t state_r;
  logic [7:0] receive_data_c;
  logic [7:0] receive_data_r;
  logic receive_data_ce;
  logic c0_ce;
  logic c1_ce;
  logic c1_ov;
  logic new_data;
  logic receive_end;
  logic [W-1:0] tic_q;
  logic tic;

  assign new_data = (state_r == IDLE) && (rx == 1'b0);
  assign state_c  = new_data ? RECEIVE :
                 receive_end ? IDLE :
                               state_r;

  always_ff @(posedge clk)
    if (rst) state_r <= IDLE;
    else state_r <= state_c;

  assign receive_data_c  = {rx, receive_data_r[7:1]};
  assign receive_data_ce = (tic_q == N / 2);

  always_ff @(posedge clk)
    if (rst) receive_data_r <= '0;
    else if (receive_data_ce) receive_data_r <= receive_data_c;

  assign c0_ce = (state_r == RECEIVE);

  counter #(
    .N(N)
  ) c0 (
    .clk(clk),
    .rst(rst),
    .ce (c0_ce),
    .q  (tic_q),
    .ov (tic)
  );

  assign c1_ce = c0_ce & tic;

  counter #(
    .N(9)
  ) c1 (
    .clk(clk),
    .rst(rst),
    .ce (c1_ce),
    .q  (),
    .ov (c1_ov)
  );

  assign receive_end = c1_ov & c1_ce;

  always_ff @(posedge clk)
    if (rst) data <= '0;
    else if (receive_end) data <= receive_data_r;

  always_ff @(posedge clk)
    if (rst) valid <= '0;
    else valid <= receive_end;

endmodule

`default_nettype wire
