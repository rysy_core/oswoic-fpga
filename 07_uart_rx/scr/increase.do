#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog increase.sv
vlog increase_tb.sv

vsim work.increase_tb

add wave -divider "input"
add wave sim:/increase_tb/dut/clk
add wave sim:/increase_tb/dut/rst
add wave -unsigned sim:/increase_tb/dut/data_in
add wave sim:/increase_tb/dut/valid_in
add wave sim:/increase_tb/dut/button

add wave -divider "inside"
add wave sim:/increase_tb/dut/button_r
add wave sim:/increase_tb/dut/valid_in_r
add wave -unsigned sim:/increase_tb/dut/data_in_r
add wave -unsigned sim:/increase_tb/dut/data_c

add wave -divider "output"
add wave -unsigned sim:/increase_tb/dut/data_out
add wave sim:/increase_tb/dut/valid_out

run -all
wave zoom full
