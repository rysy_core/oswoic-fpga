/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module increase_tb;
  logic clk;
  logic rst;
  logic [7:0] data;
  logic valid;
  logic button;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst = 1'b1;
    data = 8'd0;
    valid = 1'b0;
    button = 1'b0;
    @(posedge clk);
    rst   = 1'b0;
    valid = 1'b1;
    data  = 8'd23;
    @(posedge clk);
    valid = 1'b0;
    @(posedge clk);
    valid  = 1'b1;
    data   = 8'd100;
    button = 1'b1;
    @(posedge clk);
    valid = 1'b0;
    repeat (3) @(posedge clk);
    $stop;
  end

  increase dut (
    .clk(clk),
    .rst(rst),
    .data_in(data),
    .valid_in(valid),
    .button(button),
    .data_out(),
    .valid_out()
  );

endmodule

`default_nettype wire
