#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../02_counter/src/counter.sv
vlog ../../06_uart_tx/scr/uart_tx.sv
vlog uart_rx.sv
vlog uart_rx_tb.sv

vsim work.uart_rx_tb

add wave sim:/uart_rx_tb/dut/clk
add wave sim:/uart_rx_tb/dut/rst

add wave -divider "send"
add wave sim:/uart_rx_tb/dut/data
add wave sim:/uart_rx_tb/dut/valid
add wave sim:/uart_rx_tb/dut/rx

add wave -divider "state"
add wave sim:/uart_rx_tb/dut/state_c
add wave sim:/uart_rx_tb/dut/state_r
add wave sim:/uart_rx_tb/dut/new_data
add wave sim:/uart_rx_tb/dut/receive_data_c
add wave sim:/uart_rx_tb/dut/receive_data_r
add wave sim:/uart_rx_tb/dut/receive_data_ce
add wave sim:/uart_rx_tb/dut/c0_ce
add wave sim:/uart_rx_tb/dut/tic
add wave sim:/uart_rx_tb/dut/c1/ce
add wave sim:/uart_rx_tb/dut/c1/q
add wave sim:/uart_rx_tb/dut/receive_end

add wave -divider "outputs"
add wave sim:/uart_rx_tb/dut/data
add wave sim:/uart_rx_tb/dut/valid

run -all
wave zoom full
