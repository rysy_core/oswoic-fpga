/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module increase (
  input wire clk,
  input wire rst,
  input wire [7:0] data_in,
  input wire valid_in,
  input wire button,
  output logic [7:0] data_out,
  output logic valid_out
);

  logic button_r;
  logic valid_in_r;
  logic [7:0] data_in_r;
  logic [8:0] data_c;

  always_ff @(posedge clk)
    if (rst) button_r <= '0;
    else button_r <= button;

  always_ff @(posedge clk)
    if (rst) valid_in_r <= '0;
    else valid_in_r <= valid_in;

  always_ff @(posedge clk)
    if (rst) data_in_r <= '0;
    else data_in_r <= data_in;

  always_ff @(posedge clk)
    if (rst) valid_out <= '0;
    else valid_out <= valid_in_r;

  assign data_c = button_r ? data_in_r + 8'd1 : data_in_r + 8'd2;

  always_ff @(posedge clk)
    if (rst) data_out <= '0;
    else data_out <= data_c[7:0];

endmodule

`default_nettype wire
