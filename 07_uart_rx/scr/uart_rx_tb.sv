/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module uart_rx_tb;
  parameter FCLK = 27000000;
  parameter BAUD = 115200;

  logic clk;
  logic rst;
  logic [7:0] data;
  logic valid;
  logic tx;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst   = 1'b1;
    data  = 8'b10101000;
    valid = 1'b0;
    repeat (2) @(posedge clk);
    rst = 1'b0;
    repeat (2) @(posedge clk);

    valid = 1'b1;
    @(posedge clk);
    valid = 1'b0;
    repeat (2500) @(posedge clk);

    $stop;
  end

  uart_tx #(
    .FCLK(FCLK),
    .BAUD(BAUD)
  ) utx (
    .clk(clk),
    .rst(rst),
    .data(data),
    .valid(valid),
    .ready(),
    .tx(tx)
  );

  uart_rx #(
    .FCLK(FCLK),
    .BAUD(BAUD)
  ) dut (
    .clk(clk),
    .rst(rst),
    .rx(tx),
    .data(),
    .valid()
  );

endmodule

`default_nettype wire
