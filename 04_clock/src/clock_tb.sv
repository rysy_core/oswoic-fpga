/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module clock_tb;
  logic clk;
  logic a_in;
  logic b_min;
  logic b_hour;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    a_in   = 1'b0;
    b_min  = 1'b1;
    b_hour = 1'b1;
    repeat (5) @(posedge clk);
    a_in  = 1'b1;

    b_min = 1'b0;
    repeat (1500 * 12) @(posedge clk);
    b_min  = 1'b1;

    b_hour = 1'b0;
    repeat (1500 * 12) @(posedge clk);
    b_hour = 1'b1;

    a_in   = 1'b0;
    repeat (5) @(posedge clk);
    a_in = 1'b1;

    repeat (1500 * 60 * 60 * 13) @(posedge clk);

    $stop;
  end

  clock #(
    .FCLK(1500)
  ) dut (
    .clk(clk),
    .a_in(a_in),
    .b_min(b_min),
    .b_hour(b_hour),
    .colon(),
    .leds(),
    .select()
  );

endmodule

`default_nettype wire
