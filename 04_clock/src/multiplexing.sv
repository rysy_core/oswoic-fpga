/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module multiplexing (
  input wire clk,
  input wire rst,
  input wire ce,
  input wire [3:0][3:0] data_in,
  output logic [6:0] leds,
  output logic [3:0] select
);
  logic [2:0] cnt_c;
  logic [1:0] cnt_r;
  logic [3:0] select_c;
  logic [3:0] data_c;

  assign cnt_c = cnt_r + 1'd1;

  always_ff @(posedge clk)
    if (rst) cnt_r <= '0;
    else if (ce) cnt_r <= cnt_c[1:0];

  assign select_c = (cnt_r == 2'd0) ? 4'b1110 :
                    (cnt_r == 2'd1) ? 4'b1101 :
                    (cnt_r == 2'd2) ? 4'b1011 :
                                      4'b0111;

  always_ff @(posedge clk)
    if (rst) select <= '0;
    else select <= select_c;

  assign data_c = data_in[cnt_r];

  led_decode dut (
    .clk (clk),
    .rst (rst),
    .bdc (data_c),
    .leds(leds)
  );

endmodule

`default_nettype wire
