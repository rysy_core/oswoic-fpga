#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../02_counter/src/counter.sv
vlog ../../03_7seg/src/led_decode.sv
vlog hour_to_bcd.sv
vlog multiplexing.sv
vlog clock.sv
vlog clock_tb.sv

vsim work.clock_tb

add wave -divider "inputs"
add wave sim:/clock_tb/clk
add wave sim:/clock_tb/a_in
add wave sim:/clock_tb/b_min
add wave sim:/clock_tb/b_hour

add wave -divider "counters"
add wave sim:/clock_tb/dut/f500
add wave sim:/clock_tb/dut/ce_f1
add wave -unsigned sim:/clock_tb/dut/f1_out
add wave sim:/clock_tb/dut/ce_f60s
add wave sim:/clock_tb/dut/ce_f10m
add wave -unsigned sim:/clock_tb/dut/f10m_out
add wave sim:/clock_tb/dut/ce_f60m
add wave -unsigned sim:/clock_tb/dut/f60m_out
add wave sim:/clock_tb/dut/ce_f12h
add wave -unsigned sim:/clock_tb/dut/f12h_out

add wave -divider "time"
add wave -unsigned sim:/clock_tb/dut/min_low
add wave -unsigned sim:/clock_tb/dut/min_high
add wave -unsigned sim:/clock_tb/dut/hour_low
add wave -unsigned sim:/clock_tb/dut/hour_high

add wave -divider "outputs"
add wave sim:/clock_tb/dut/colon
add wave -unsigned sim:/clock_tb/dut/leds
add wave -unsigned sim:/clock_tb/dut/select

run -all
wave zoom full
