/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module hour_to_bcd (
  input wire clk,
  input wire rst,
  input wire [3:0] hour,
  output logic [3:0] low,
  output logic [3:0] high
);
  logic signed [4:0] difference_c;
  logic [3:0] low_c;
  logic [3:0] high_c;

  assign difference_c = hour - 4'd10;

  assign low_c = difference_c[4] ? hour :
                                   difference_c[3:0];
  assign high_c = difference_c[4] ? 4'd0 : 4'd1;

  always_ff @(posedge clk)
    if (rst) low <= '0;
    else low <= low_c;

  always_ff @(posedge clk)
    if (rst) high <= '0;
    else high <= high_c;

endmodule

`default_nettype wire
