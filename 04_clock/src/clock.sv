/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module clock #(
  parameter FCLK = 27000000
) (
  input wire clk,
  input wire a_in,
  input wire b_min,
  input wire b_hour,
  output logic colon,
  output logic [6:0] leds,
  output logic [3:0] select
);
  logic b_min_r;
  logic b_hour_r;
  logic rst;
  logic f500;
  logic f10;
  logic ce_f1;
  logic f1;
  logic [3:0] f1_out;
  logic f60s;
  logic ce_f60s;
  logic f10m;
  logic ce_f10m;
  logic [3:0] f10m_out;
  logic f60m;
  logic ce_f60m;
  logic [2:0] f60m_out;
  logic ce_f12h;
  logic [3:0] f12h_out;
  logic [3:0] min_low;
  logic [3:0] min_high;
  logic [3:0] hour_low;
  logic [3:0] hour_high;
  logic colon_s;

  always_ff @(posedge clk) rst <= ~a_in;

  always_ff @(posedge clk)
    if (rst) b_min_r <= '1;
    else b_min_r <= b_min;

  always_ff @(posedge clk)
    if (rst) b_hour_r <= '1;
    else b_hour_r <= b_hour;

  counter #(
    .N(FCLK / 500)
  ) c_500Hz (
    .clk(clk),
    .rst(rst),
    .ce (1'b1),
    .q  (),
    .ov (f500)
  );

  counter #(
    .N(50)
  ) c_10Hz (
    .clk(clk),
    .rst(rst),
    .ce (f500),
    .q  (),
    .ov (f10)
  );

  assign ce_f1 = f500 & f10;

  counter #(
    .N(10)
  ) c_1Hz (
    .clk(clk),
    .rst(rst),
    .ce (ce_f1),
    .q  (f1_out),
    .ov (f1)
  );

  assign ce_f60s = f1 & ce_f1;

  counter #(
    .N(60)
  ) c_60s (
    .clk(clk),
    .rst(rst),
    .ce (ce_f60s),
    .q  (),
    .ov (f60s)
  );

  assign ce_f10m = (b_min_r == 1'd0) ? ce_f1 : f60s & ce_f60s;

  counter #(
    .N(10)
  ) c_10m (
    .clk(clk),
    .rst(rst),
    .ce (ce_f10m),
    .q  (f10m_out),
    .ov (f10m)
  );

  assign ce_f60m = f10m & ce_f10m;

  counter #(
    .N(6)
  ) c_60m (
    .clk(clk),
    .rst(rst),
    .ce (ce_f60m),
    .q  (f60m_out),
    .ov (f60m)
  );

  assign ce_f12h = (b_hour_r == 1'd0) ? ce_f60s :
                    (b_min_r == 1'd0) ? 1'd0 :
                                        f60m & ce_f60m;

  counter #(
    .N(12)
  ) c_12h (
    .clk(clk),
    .rst(rst),
    .ce (ce_f12h),
    .q  (f12h_out),
    .ov ()
  );

  always_ff @(posedge clk)
    if (rst) min_low <= '0;
    else min_low <= f10m_out;

  always_ff @(posedge clk)
    if (rst) min_high <= '0;
    else min_high <= f60m_out;

  hour_to_bcd htb (
    .clk (clk),
    .rst (rst),
    .hour(f12h_out),
    .low (hour_low),
    .high(hour_high)
  );

  multiplexing m (
    .clk(clk),
    .rst(rst),
    .ce(f500),
    .data_in({min_low, min_high, hour_low, hour_high}),
    .leds(leds),
    .select(select)
  );

  assign colon_s = f1_out > 4'd4;

  always_ff @(posedge clk)
    if (rst) colon <= 1'b1;
    else colon <= colon_s;

endmodule

`default_nettype wire
