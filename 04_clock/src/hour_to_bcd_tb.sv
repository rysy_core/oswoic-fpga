/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module hour_to_bcd_tb;
  logic clk;
  logic rst;
  logic [3:0] hour;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst  = 1'b1;
    hour = 4'b0;
    @(posedge clk);
    rst = 1'b0;
    for (integer i = 0; i < 12; i++) begin
      hour = i[3:0];
      @(posedge clk);
    end
    @(posedge clk);
    $stop;
  end

  hour_to_bcd dut (
    .clk (clk),
    .rst (rst),
    .hour(hour),
    .low (),
    .high()
  );

endmodule

`default_nettype wire
