#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog hour_to_bcd.sv
vlog hour_to_bcd_tb.sv

vsim work.hour_to_bcd_tb

add wave -divider "inputs"
add wave sim:/hour_to_bcd_tb/clk
add wave sim:/hour_to_bcd_tb/rst
add wave -unsigned sim:/hour_to_bcd_tb/hour

add wave -divider "module inside"
add wave -signed sim:/hour_to_bcd_tb/dut/difference_c
add wave sim:/hour_to_bcd_tb/dut/difference_c[4]
add wave -unsigned sim:/hour_to_bcd_tb/dut/low_c
add wave -unsigned sim:/hour_to_bcd_tb/dut/high_c

add wave -divider "outputs"
add wave -unsigned sim:/hour_to_bcd_tb/dut/low
add wave -unsigned sim:/hour_to_bcd_tb/dut/high

run -all
wave zoom full
