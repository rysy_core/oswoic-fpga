/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2023 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module multiplexing_tb;
  logic clk;
  logic rst;
  logic [3:0][3:0] data_in;
  logic ce;

  initial begin
    clk = 1'b0;
    forever #5 clk = ~clk;
  end

  initial begin
    rst = 1'b1;
    data_in = {4'd0, 4'd1, 4'd2, 4'd3};
    @(posedge clk);
    rst = 1'b0;

    repeat (5) begin
      ce = 1'b0;
      repeat (3) @(posedge clk);
      ce = 1'b1;
      @(posedge clk);
    end

    @(posedge clk);
    $stop;
  end

  multiplexing dut (
    .clk(clk),
    .rst(rst),
    .ce(ce),
    .data_in(data_in),
    .leds(),
    .select()
  );

endmodule

`default_nettype wire
