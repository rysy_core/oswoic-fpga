#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2021 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../../03_7seg/src/led_decode.sv
vlog multiplexing.sv
vlog multiplexing_tb.sv

vsim work.multiplexing_tb

add wave -divider "inputs"
add wave sim:/multiplexing_tb/clk
add wave sim:/multiplexing_tb/rst
add wave -unsigned sim:/multiplexing_tb/data_in
add wave sim:/multiplexing_tb/ce

add wave -divider "module inside"
add wave -unsigned sim:/multiplexing_tb/dut/cnt_c
add wave -unsigned sim:/multiplexing_tb/dut/cnt_r
add wave sim:/multiplexing_tb/dut/select_c
add wave -unsigned sim:/multiplexing_tb/dut/data_c

add wave -divider "outputs"
add wave sim:/multiplexing_tb/dut/leds
add wave sim:/multiplexing_tb/dut/select

run -all
wave zoom full
